1. Build with Maven using

clean package

2. Launch resulting war from the command line.

java -jar zadanie-1.1.war

3. Then go to localhost in the browser, port 8080

It's very basic, but I was told it is not necessarily a downside.
Interface is bare minimum.