package com.techminds.zadanie.controller.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public abstract class DataFileParser {
        protected MultipartFile file;
        protected List<List<String>> dataCells;

        public DataFileParser(MultipartFile afile) throws IOException {
            file = afile;
            dataCells = processFile(file);
        }

        public List<List<String>> getDataCells() {
            return dataCells;
        }

        protected abstract List<List<String>> processFile(MultipartFile afile) throws IOException;

    }
