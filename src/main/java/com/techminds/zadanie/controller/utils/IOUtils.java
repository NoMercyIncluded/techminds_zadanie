package com.techminds.zadanie.controller.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IOUtils {

    public static List<String> readLines(MultipartFile file) throws IOException {
        List result = new ArrayList<>();
        BufferedReader br;

        String line;
        InputStream is = file.getInputStream();
        br = new BufferedReader(new InputStreamReader(is));

        while ((line = br.readLine()) != null) {
            result.add(line);
        }

        return result;
    }

    public static String getFileExtension(MultipartFile file){
        String filename = file.getOriginalFilename();
        String extension = "";
        try {
            extension = filename.substring(filename.lastIndexOf(".")+1);
        } catch (Exception e){
        }

        return extension;
    }

}
