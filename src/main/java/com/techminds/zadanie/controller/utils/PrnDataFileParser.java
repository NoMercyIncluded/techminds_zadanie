package com.techminds.zadanie.controller.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.techminds.zadanie.controller.utils.IOUtils.readLines;

public class PrnDataFileParser extends DataFileParser {

    public PrnDataFileParser(MultipartFile afile) throws IOException {
        super(afile);
    }

    @Override
    protected List<List<String>> processFile(MultipartFile afile) throws IOException {
        List<String> lines = readLines(file);
        List result = new ArrayList<>();
        //I know I could do inverse if (size>0 then do logic) but I don't want more depth, this way it's more readable
        if (lines.size() < 1) {
            return result;
        }

        Integer[] cellIndex = calculateCells(lines);

        return cutColumns(lines, cellIndex);
    }

    //calculate beginning of the cell
    private Integer[] calculateCells(List<String> lines) {
        char[] line = lines.get(0).toCharArray();
        List<Integer> initialList = new ArrayList<>();
        List<Integer> doubleCheck = new ArrayList<>();

        char previous = ' ';
        boolean multipleSpace = true;

        for (int i = 0; i < line.length; i++) {
            if (line[i] != ' ' && previous == ' ') {
                initialList.add(i);
                //if there is just single space between other characters we need to double check if it's new line or just multi-word column header
                if (!multipleSpace) {
                    doubleCheck.add(i);
                }
                multipleSpace = false;
            } else if (line[i] == ' ' && previous == ' ' && multipleSpace == false) {
                multipleSpace = true;
            }
            previous = line[i];
        }

        Iterator<Integer> itr;
        Integer index;
        if (!doubleCheck.isEmpty()) {
            for (String l : lines) {
                itr = doubleCheck.iterator();
                while (itr.hasNext()) {
                    index = itr.next();
                    if (l.charAt(index - 1) != ' ') {
                        itr.remove();
                        initialList.remove(index);
                    }
                }
            }
        }

        return initialList.toArray(new Integer[0]);
    }

    // Separated this logic to reuse it in version utilizing parsing into type with hardcoded fields;
    private List<List<String>> cutColumns(List<String> lines, Integer[] cellIndex) {
        List result = new ArrayList<>();

        int cellEnd = 0;
        for (String line : lines) {
            List<String> cells = new ArrayList<>();
            for (int i = 0; i < cellIndex.length; i++) {
                if (i == cellIndex.length - 1) {
                    cellEnd = line.length();
                } else {
                    cellEnd = cellIndex[i + 1];
                }
                //cut data cell and trim it off spaces
                cells.add(line.substring(cellIndex[i], cellEnd).trim());
            }
            result.add(cells);
        }
        return result;
    }
}
