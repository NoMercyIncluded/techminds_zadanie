package com.techminds.zadanie.controller.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.techminds.zadanie.controller.utils.IOUtils.readLines;

public class CsvDataFileParser extends DataFileParser {

    public CsvDataFileParser(MultipartFile afile) throws IOException {
        super(afile);
    }

    @Override
    protected List<List<String>> processFile(MultipartFile afile) throws IOException {
        List<String> lines = readLines(file);
        List result = new ArrayList<>();
        //I know I could do inverse if (size>0 then do logic) but I don't want more depth, this way it's more readable
        if (lines.size() < 1) {
            return result;
        }

        for (String line : lines) {
            List<String> cells;
            String separated;
            StringBuilder str = new StringBuilder(line);

            //dealing with commas in data - converting separators to tabs
            boolean isQuote = false;
            for (int i = 0; i < line.length(); i++) {
                if (line.charAt(i) == ',' && !isQuote) {
                    str.replace(i, i + 1, "\t");
                } else if (line.charAt(i) == '\"') {
                    isQuote = !isQuote;
                }
            }

            separated = str.toString();
            cells = Arrays.asList(separated.split("\t"));
            result.add(cells);
        }

        return result;
    }

}
