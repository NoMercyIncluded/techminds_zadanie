package com.techminds.zadanie.controller;

import com.techminds.zadanie.controller.utils.CsvDataFileParser;
import com.techminds.zadanie.controller.utils.DataFileParser;
import com.techminds.zadanie.controller.utils.IOUtils;
import com.techminds.zadanie.controller.utils.PrnDataFileParser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@Controller
public class UploadController {

    public static final String csvExtension = "csv";
    public static final String prnExtension = "prn";

    private List<List<String>> fileContents;

    @GetMapping("/")
    public String index() {
        return "upload";
    }

    @PostMapping("/upload")
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        fileContents = null;
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {
            String fileExtension = IOUtils.getFileExtension(file);
            redirectAttributes.addFlashAttribute("fileExtension", IOUtils.getFileExtension(file));

            DataFileParser parser;
            //I guess we can just throw FileNotFound in case of unknown extension, no need to create another exception type
            if(fileExtension.toLowerCase().equals(csvExtension)){
                parser = new CsvDataFileParser(file);
            } else if(fileExtension.toLowerCase().equals(prnExtension)){
                parser = new PrnDataFileParser(file);
            } else throw new FileNotFoundException("Selected file is not of CSV or PRN extension!");

            fileContents = parser.getDataCells();

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }

        return "redirect:/uploadStatus";
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus(Model model) {
        model.addAttribute("fileContents", fileContents);
        return "uploadStatus";
    }

}
